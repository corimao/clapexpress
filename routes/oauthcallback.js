var express = require('express');
var router = express.Router();
var google = require('googleapis');
var OAuth2 = google.auth.OAuth2;

var oauth2Client = new OAuth2(
    gApp.oAuth.client_id,
    gApp.oAuth.client_secret,
    gApp.oAuth.redirect_uri
);

/* GET users listing. */
router.get('/', function (req, res, next) {
    console.log(req.query.code);
    var code = req.query.code || undefined;
    if (code){
        oauth2Client.getToken(code, function (err, tokens) {
            if (!err) {                                            // Now tokens contains an access_token and an optional refresh_token. Save them.
                oauth2Client.setCredentials(tokens);

                console.log(tokens);
                gApp.oAuth.access_token = tokens.access_token;
                gApp.oAuth.refresh_token = tokens.refresh_token;
                gApp.oAuth.token_type = tokens.token_type;
                gApp.oAuth.expiry_date = tokens.expiry_date;

                res.send('respond with a resource');
            }else{
                console.log(err);
                res.send(406, err);
            }
        });
    }
});


router.get('/geturl', function (req, res, next) {
    var url = oauth2Client.generateAuthUrl({
        // 'online' (default) or 'offline' (gets refresh_token)
        access_type: 'offline',

        // If you only need one scope you can pass it as string
        scope: [
            'https://www.googleapis.com/auth/cloudprint'
        ]
    });
    console.log(url);
    res.redirect(url);
});


module.exports = router;
