/**
 * Created by jmb on 13/11/2016.
 */

var express = require('express');
var router = express.Router();

var CloudPrint = require('../lib/cloud-print');
var cloudprint = new CloudPrint({
    service_provider: 'google',
    auth: gApp.oAuth
});

var getPrinters = function(pCallBack){
    var tabPrinters = [];
    cloudprint.getPrinters({}, function (err, response) {
        if (err) {
            console.log('getPrinters err: ' + err);
        } else {
            var getPrinter = JSON.parse(response);

            getPrinter.printers.forEach(function(index, number){
                var l_Printer = {};
                l_Printer.displayName = getPrinter.printers[number].displayName;
                l_Printer.connectionStatus = getPrinter.printers[number].connectionStatus;
                l_Printer.id = getPrinter.printers[number].id;

                tabPrinters.push(l_Printer);
            });
        }
        if (pCallBack) {pCallBack(err, tabPrinters)}
    });

};

/* GET users listing. */
router.get('/', function (req, res, next) {
    getPrinters(function(err, reponse){
        if (err) {
            res.send(406, err);
        } else {
            var option = {
                title: 'Impression',
                printers: reponse
            };
            res.render('cloudprint', option);
        }
    })
});

router.get('/getPrinters', function (req, res, next) {
    getPrinters(function(err, reponse){
        if (err) {
            res.send(406, err);
        } else {
            res.send(reponse);
        }
    })
});

router.post('/print', function (req, res, next) {
    var id = req.query.id || '__google__docs';
    var title = req.body.title || 'Image CLAP_mapad1';
    var content = req.body.content || 'http://clap-box.fr/DemoData/CLAP_mapad.ech_20161109133437.png';
    var copies = parseInt(req.body.copies) || 1;
    var page_orientation = parseInt(req.body.page_orientation) || 1;

    var params = {
        title: title,
        content: content,
        contentType : 'url',
        printer_id : id,
        settings :{
            "copies":{"copies":copies},
            "page_orientation":{"type":page_orientation}
        }
    };

    cloudprint.print(params, function (err, response) {
        if (err){
            console.log('getPrinters err: ' + err);
            res.send(406, err);
        }else{
            console.log(response);
            var l_ORep = JSON.parse(response);
            if(l_ORep.errorCode){
                res.send(406, {errorCode: l_ORep.errorCode, message: l_ORep.message});
            }else{
                res.status(200);
            }
        }
    });
});

module.exports = router;
