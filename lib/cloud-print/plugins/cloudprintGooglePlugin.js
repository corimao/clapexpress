/**
 * Created by jmb on 05/11/2016.
 */

const google = require('googleapis');
const https = require('https');
const querystring = require('querystring');
const url = require('url');

var oauth2Client;

function _getToken(pCode, pCallBack) {
    oauth2Client.getToken(pCode, function (err, tokens) {
        // Now tokens contains an access_token and an optional refresh_token. Save them.
        if (!err) {
            token = tokens;
            oauth2Client.setCredentials(tokens);
            console.log(tokens);
        } else {
            console.log(err)
        }
        if (pCallBack){pCallBack()}
    });
}

function _refreshAccessToken(pCallBack){
    oauth2Client.refreshAccessToken(function(err, tokens) {
        // your access_token is now refreshed and stored in oauth2Client
        // store these new tokens in a safe place (e.g. database)
        if (!err) {
            token = tokens;
        }else{
            console.log(err)
        }
        if (pCallBack){pCallBack(err)}
    });
}


function _getHttp(options, callback){
    var l_Data = {};
    var authorization = token.token_type +' '+token.access_token;       //'Bearer ya29.Ci-MA1KuU5JcetHoObtCWddROiUH_wo-DF5WqFuBVcEn7ervN1AtgKde5k148uFoPA';

    if (options.postData){
        l_Data.printerid = options.postData.printerid;
        l_Data.title = options.postData.title;
        l_Data.content = options.postData.content;
        l_Data.contentType = options.postData.contentType;
        l_Data.ticket = options.postData.ticket;
    }
    var postData = querystring.stringify(l_Data);

    var dataReponse = '';
    var l_Option = {};
    l_Option.host = url.parse(options.url).host;
    l_Option.path = url.parse(options.url).pathname;
    l_Option.method = options.method;
    l_Option.headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(postData),
        'Authorization': authorization
    };
    var req = https.request(l_Option, function(res){
        res.on('data', function(retour){
            dataReponse += retour;
        });
        res.on('end', function(){
            if (callback){callback(null, dataReponse)}
        });
        res.on('error', function (err) {
            console.log(err);
            if (callback){callback(err)}
        })
    });

    // Bypass le timeout pour mieux contrôler le délai
    req.setTimeout(10000, function(){
        if (callback){callback('TimeOut')}
    });

    req.on("error", function(e){
        if (callback){callback(e)}
    });

    req.write(postData);                                        // Send request witht the postData form
    req.end();                                                  // Send end request witht the postData form
};

var _envoiCmd = function(pParametre, pCallBack){
    _refreshAccessToken(function(err){
        if(!err){
            _getHttp(pParametre, pCallBack);
        }else{
            if (pCallBack){pCallBack(err)}                       // Appel la callback en renvoyant l'err
        }
    })
};

module.exports = CloudPrint;

function CloudPrint(options) {
    var self = this;
    this._options = options || {};

    if (options.auth) {
        var auth = options.auth;
        oauth2Client = new google.auth.OAuth2(auth.client_id, auth.client_secret, auth.redirect_uri);
        oauth2Client.setCredentials({
            access_token: auth.access_token,
            refresh_token: auth.refresh_token
        });
    }

    this.getPrinters = function(params, callback) {
        var parameters = {
            url : 'https://www.google.com/cloudprint/search',
            method: 'POST',
            params: params
        };
        _envoiCmd(parameters, callback);
    };

    this.getPrinter = function (printer_id, callback) {
        var parameters = {
            url : 'https://www.google.com/cloudprint/printer',
            method: 'POST',
            postData: {printerid: printer_id}
        };

        _envoiCmd(parameters, callback);
    };

    this.print = function(params, callback) {
        var ticket = {
            version: '1.0',
            print: params.settings
        };

        // ticket = {
        //     version: '1.0',
        //     print: {
        //         color:{
        //             vendor_id:"1",
        //             type:1
        //         },
        //         duplex:{type:0},
        //         page_orientation:{type:0},
        //         copies:{copies:"2"},
        //         fit_to_page:{type:3},
        //         page_range:{interval:[{start: 1,end:2}]},
        //         media_size:{
        //             width_microns:210000,
        //             height_microns:297000,
        //             is_continuous_feed:false,
        //             vendor_id:"9"
        //         },
        //         collate:{collate:false},
        //         reverse_order:{reverse_order:false}
        //     }
        // };

        //ticket = "{\"version\":\"1.0\",\"print\":{\"color\":{\"vendor_id\":\"1\",\"type\":1},\"duplex\":{\"type\":0},\"page_orientation\":{\"type\":0},\"copies\":{\"copies\": 2},\"fit_to_page\":{\"type\":3},\"page_range\":{\"interval\":[{\"start\": 2,\"end\":5}]},\"media_size\":{\"width_microns\":210000,\"height_microns\":297000,\"is_continuous_feed\":false,\"vendor_id\":\"9\"},\"collate\":{\"collate\":false},\"reverse_order\":{\"reverse_order\":false}}}";
        // var ticket1 = "{\"version\":\"1.0\",\"print\":{\"color\":{\"vendor_id\":\"1\",\"type\":1},\"duplex\":{\"type\":0},\"page_orientation\":{\"type\":0},\"copies\":{\"copies\":\"2\"},\"fit_to_page\":{\"type\":3},\"page_range\":{\"interval\":[{\"start\":1,\"end\":2}]},\"media_size\":{\"width_microns\":210000,\"height_microns\":297000,\"is_continuous_feed\":false,\"vendor_id\":\"9\"},\"collate\":{\"collate\":false},\"reverse_order\":{\"reverse_order\":false}}}";
        console.log(JSON.stringify(ticket));

        var postData = {
            ticket: JSON.stringify(ticket),
            // ticket: ticket1,
            printerid: params.printer_id,
            title: params.title,
            content: params.content,
            contentType: params.contentType
        };

        var parameters = {
            url : 'https://www.google.com/cloudprint/submit',
            method: 'POST',
            params: params,
            postData : postData
        };
        _envoiCmd(parameters, callback);
    };
}

var l_Ticket = {
    version: '1.0',
    print: {
        page_orientation:{type:1},
        fit_to_page: {type:1}
    }
};

var ticket = {
    "version":"1.0",
    "print":{
        "copies":{"copies":1},
        "page_orientation":{"type":0},
        "margins": {
            "top_microns":0,
            "bottom_microns":0,
            "left_microns":0,
            "right_microns":0
        },
        "fit_to_page": {"type": "NO_FITTING"},
        "dpi": {
            "horizontal_dpi":203,
            "vertical_dpi":203
        },
        "media_size": {
            "width_microns": 100000,
            "height_microns": 37000,
            "is_continuous_feed": 1
        }
    }
};